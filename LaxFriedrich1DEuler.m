% Lax Friedrich scheme to solve 1D Euler problem

clear all;
clc;

% Input parameters

M1   = 2.5;
CFL  =  0.6; 
alpha = 0.5;

n       = 200;             % Number of grid points
nt      = 10000;           % Number of iterations


U       = zeros(3,n);
F       = zeros(3,n);
x       = zeros(1,n);
rho     = zeros(1,n);
rhoe    = zeros(1,n);
u       = zeros(1,n);
p       = zeros(1,n);
T       = zeros(1,n);
a       = zeros(1,n);

gama = 1.4;
gm1  = gama - 1;
gp1  = gama + 1;
Rg   = 1 / gama;                                    
Cv   = 1 / ( gama * gm1 );                           
Cp   = 1 / gm1;      

% Normalized by (gama*R)
% Normalization : rho = rho*/rho1;   u= u*/a1; p = p*/(rho1*a1**2);
% T=T*/T1; Cv=Cv*/(gama*R); R = R*/(gama*R*)

% Mean values for variables

M2   = sqrt( ( M1^2 * gm1 + 2 ) / ( 2 * gama * M1^2 - gm1 ) );                               % Normal shock relation

P1   = 1 / gama;                                                                             % Normalized by (gamma*itself)
P2   = P1 * ( 1 + ( 2 * gama * ( M1^2 - 1 ) / gp1 ) );                                       % Normal shock relation

T1   = 1;                                                                                    % Normalized by itself  
T2   = T1 * ( 1 + ( (2 * gm1) * (gama * M1^2 + 1) * ( M1^2 - 1 ) ) / ( gp1^2 * M1^2 ) );     % Normal shock relation

a1   = sqrt( gama * Rg * T1 );
a2   = sqrt( gama * Rg * T2 );

U1   = M1;                                       % Normalized Velocity 
U2   = M2 * a2 / a1;                             % Normalized downstream velocity

rho1 = 1;                                        % Normalized by itself
rho2 = U1 / U2 * rho1;                           % Normalized downstream density


% Grid spacing and time step details

x_min   = -1;                                 
x_max   = 5;                                  
Length  = x_max - x_min;    
x_shk   = 0;
dx      = Length / n;   
dt      = (U1 + a1);                             % Initial value for timestep size


% Initialization for all the values

for i = 1:n
    
    x(i) = x_min + ( i - 1 ) * dx;
    
    if(x(i) < x_shk)
        
        rho(i)   = rho1;
        u(i)     = U1;
        p(i)     = P1;
        T(i)     = T1; 
        
    else
        
        p(i)     = P2;
        rho(i)   = rho2;
        u(i)     = U2;
        T(i)     = T2;
        
    end
    
    rhoe(i)      = p(i) / gm1 + 0.5 * rho(i) .* u(i).^2; 
    a(i)         = sqrt( gama * p(i) ./ rho(i) );
    
end


        
step1 = 0;
t     = 0;


while(t < nt * dt)

    % Computing the timestep based on largest eigen value
    
    dt  = CFL * dx / (U1+a1);
    
    
    % defining the matrices
    
    U = [rho; rho.*u; rhoe];
    
    F = [rho.*u; rho.*u.^2 + p; u.*(rhoe+p)];
   
    
    % update interior points, conserved variables
    
    f(1:3,1:n-1) = 0.5 * ( F(1:3,1:n-1) + F(1:3,2:n) ) - 0.5 * alpha.*(U1+a1)* ( U(1:3,2:n) - U(1:3,1:n-1) );
    U(1:3,2:n-1) = U(1:3,2:n-1) - dt / dx * ( f(1:3,2:n-1) - f(1:3,1:n-2) );                                                      
    
    for i=2:n-1
        
        rho(i)    = U(1,i);
        u(i)      = U(2,i) ./ rho(i);
        rhoe(i)   = U(3,i); 
        p(i)      = gm1 * (rhoe(i) - 0.5 * rho(i) .* u(i).^2);
        a(i)      = sqrt( gama * p(i) ./ rho(i) );
        T(i)      = p(i) ./ ( Rg * rho(i) );
        
    end
    
    % Time and step update
    
    t     = t + dt;
    step1 = step1 + 1;


        fprintf('Count %d \n', step1);                % Printing iteration count to the screen
   

end

 
    

figure(1);
subplot(221); plot(x,rho,'-','DisplayName',num2str(CFL)); xlabel('x'); ylabel('\rho'); xlim('auto'); ylim('auto'); title('Density profile'); legend('-DynamicLegend'); hold all;
subplot(222); plot(x,u,'-','DisplayName',num2str(CFL)); xlabel('x'); ylabel('u'); xlim('auto'); ylim('auto'); title('Velocity profile'); legend('-DynamicLegend'); hold all;
subplot(223); plot(x,p,'-','DisplayName',num2str(CFL)); xlabel('x'); ylabel('p'); xlim('auto'); ylim('auto'); title('Pressure profile'); legend('-DynamicLegend'); hold all;
subplot(224); plot(x,T,'-','DisplayName',num2str(CFL)); xlabel('x'); ylabel('T'); xlim('auto'); ylim('auto'); title('Temperature profile'); legend('-DynamicLegend'); hold all;

